package com.openweb.employeeclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.openweb.employeeclient.model.Employee;
import com.openweb.employeeclient.model.EmployeeResponse;

@Component
public class EmployeeRestClient {
	
	@Autowired
    private RestTemplate employeeClientTemplate;
	private static final String OPENWEB_EMPLOYEE_JSON= "http://sys4.open-web.nl/employees.json";
	
    public List<Employee> getAllEmployees() {
       List<Employee> employeeList=employeeClientTemplate.getForObject(OPENWEB_EMPLOYEE_JSON,EmployeeResponse.class).getEmployeeList();
       return employeeList;
    }	

}
