package com.openweb.employeeclient.model;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Employee {

	@JsonProperty("name")
	private String name;
	@JsonProperty("role")
	private String role;
	@JsonProperty("skills")
	private Set<String> skills;
	@JsonProperty("profileImage")
	private String profileImage;

	
	public Employee(String name, String role, Set<String> skills, String profileImage) {
		super();
		this.name = name;
		this.role = role;
		this.skills = skills;
		this.profileImage = profileImage;
	}

	public Employee(){		
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRole() {
		return role;
	}
	
	public Set<String> getSkills() {
		return skills;
	}

	public void setSkills(Set<String> skills) {
		this.skills = skills;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public String getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
	
	
}
