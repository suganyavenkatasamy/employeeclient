package com.openweb.employeeclient.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeeResponse {
	
	  public EmployeeResponse() {
	
	}

	@JsonProperty("employees")
	   private List<Employee> employeeList;

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	  
	  

}
