package com.openweb.employeeclient.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.openweb.employeeclient.EmployeeRestClient;
import com.openweb.employeeclient.model.Employee;

@Controller
public class EmployeeClientController {	
	@Autowired
	EmployeeRestClient empRestClient;

	@RequestMapping("/employeeOverview")
	@ResponseBody
	public List<Employee> getEmployees(@RequestParam(value="skillset", required=false, defaultValue="All") Set<String> skillset) {	
		if(skillset.contains("All"))
		return empRestClient.getAllEmployees();
		else
		{
			return empRestClient.getAllEmployees().stream().filter(employee->CollectionUtils.containsAny(employee.getSkills(),skillset)).collect(Collectors.toList());
		}
	}
	
}
