package com.openweb.employeeclient;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.openweb.employeeclient.model.Employee;
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeRestClientIntegrationTest {
	
	@Autowired
    private TestRestTemplate restTemplate;
	private List<Employee> employeeList;

	
	@Test
	public void getjavaSkilledEmployees() {
		ResponseEntity<Employee[]> responseEntity = restTemplate.getForEntity("/employeeOverview?skillset=java",Employee[].class);
		employeeList = Arrays.asList(responseEntity.getBody());
		assertTrue(employeeList.get(0).getSkills().contains("java"));
		
	}
	
	@Test
	public void getAllEmployees() {
		ResponseEntity<Employee[]> responseEntity = restTemplate.getForEntity("/employeeOverview",Employee[].class);
		employeeList = Arrays.asList(responseEntity.getBody());
		assertEquals(employeeList.get(0).getName(),"Omar Ras");
		
	}

}
