package com.openweb.employeeclient.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.openweb.employeeclient.EmployeeRestClient;
import com.openweb.employeeclient.controller.EmployeeClientController;
import com.openweb.employeeclient.model.Employee;


@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeClientController.class)
public class EmployeeClientControllerTest {
	
	 @Autowired
	 MockMvc mockMvc;
	
	@MockBean
	private EmployeeRestClient empRestClient;
	
	List<Employee> empList=Arrays.asList(new Employee("Sugaya Devi","java Developer",new HashSet<String>(Arrays.asList("java","scrum master")),"suganya.jpg"),
			new Employee("Lakshana","DevOps Engineer",new HashSet<String>(Arrays.asList("AWS","scrum master")),"Lakshana.jpg"));

	@Test
	public void testgetEmployeesSuccessfully() throws Exception {
	
	   when(empRestClient.getAllEmployees()).thenReturn(empList);
	   mockMvc.perform(get("/employeeOverview"))
	            .andExpect(status().isOk())
	            .andExpect(content().json("[{\"name\":\"Sugaya Devi\",\"role\":\"java Developer\",\"skills\":[\"java\",\"scrum master\"],\"profileImage\":\"suganya.jpg\"}"
	            		+ ",{\"name\":\"Lakshana\",\"role\":\"DevOps Engineer\",\"skills\":[\"scrum master\",\"AWS\"],\"profileImage\":\"Lakshana.jpg\"}]"));
   
	}

}
