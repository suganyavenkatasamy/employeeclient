### What is this repository for? ###

The code repository act as java web client for openweb - Employee backend service(http://sys4.open-web.nl/employees.json).

    - Implemented as a Rest service that calls the backend(http://sys4.open-web.nl/employees.json) to get all employeeRecords 
	- It also filters the employee records based on skills specified.

Set Up:

Make sure you have Maven set up and the port 8085 is free.

   mvn spring-boot:run
   
 
   http://localhost:8085/employeeOverview  - To get all employee Records
   
   http://localhost:8085/employeeOverview?skillset=java,hippo  - To filtered records based on skill set.
   
  
   
   